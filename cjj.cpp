#include <stdio.h>
#include<stdlib.h>
#include<time.h> 
void menu();
void help();
void error();
void operation_1();void one();
void operation_2();void two();
void operation_3();void three();

int main()
{
    int m;
    printf("========== 口算生成器 ==========\n欢迎使用口算生成器 :\n希望早日开学\n");
    printf("\n");
    help();
    while(1)
    {
        menu();
        scanf("%d",&m);
        switch(m)
        {
            case 1:operation_1();break;
            case 2:operation_2();break;
            case 3:operation_3();break;
            case 4:help();break;
        }
        printf("\n");
        if(m==5) break;
        if(m>5||m<1) error();
    }
    return 0;
}
void help()
{
    printf("帮助信息\n您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n二年级题目为不超过百位的乘除法;\n""三年级题目为不超过百位的加减乘除混合题目.\n");
    printf("\n");
}
void menu()
{
    printf("操作列表:\n1)一年级    2)二年级    3)三年级\n4)帮助      5)退出程序\n请输入代号:");
}
void error()
{
    printf("错误程序，请重新输入正确数值。");
    printf("\n");
 } 
 void operation_1()
 {
    printf("请输入题目个数>");
    one();
 }
  void operation_2()
 {
    printf("请输入题目个数>");
    two();
 }
  void operation_3()
 {
    printf("请输入题目个数>");
    three();
 }
  void one()
 {
    int x,a,b,c;
    scanf("%d",&x);
    printf("现在是一年级题目：\n"); 
    srand((unsigned)time(NULL));
    for(int i=1;i<=x;i++)
    {
        a=rand()%10;
        b=rand()%10;
        c=rand()%2;
        switch(c){
        	case 0:printf("%d + %d = ___",a,b);break;
        	default: printf("%d - %d = ___",a,b);
		}
		printf("\n");
     } 
 } 
 void two()
 {
    int x,a,b,c;
    scanf("%d",&x);
    printf("现在是二年级题目：\n"); 
    srand((unsigned)time(NULL));
    for(int i=1;i<=x;i++)
    {
        a=rand()%100;
        b=rand()%100;
        c=rand()%4;
        if(c==0)
        printf("%3d * %3d = ___",a,b);
        else
        printf("%3d / %3d = ___",a,b);
        printf("\n");
     } 
 }
 void three()
 {
    int x,a,b,c;
    scanf("%d",&x); 
    printf("现在是三年级题目：\n"); 
    srand((unsigned)time(NULL));
    for(int i=1;i<=x;i++)
    {
        a=rand()%100;
        b=rand()%100;
        c=rand()%4;
        switch(c)
        {
            case 0:printf("%3d + %3d = ___",a,b);break;
            case 1:printf("%3d - %3d = ___",a,b);break;
            case 2:printf("%3d * %3d = ___",a,b);break;
            case 3:printf("%3d / %3d = ___",a,b);break;
         }
        printf("\n");
     }
 }
