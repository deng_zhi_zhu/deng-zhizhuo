#include<stdio.h>
#include<string.h>
#define NAME_MAX 15
#define TEL_MAX 15
#define ADDR_MAX 20
#define PEOPLE_MAX 1000
 
typedef struct LINK
{
	char name[15];
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
}linkman;
 
typedef struct PEOPLE
{
	int num;
	linkman count[1000];
}*people;
//在通讯录里模糊搜索遍历到指定联系人
static int _search(people cou, const char *name)
{
	int number = 0;
 
	for (int i = 0; i < cou->num; i++)
	{
		if ((strstr((cou->count[i]).name, name) != NULL) || (strstr((cou->count[i]).tel, name) != NULL))
		{
			printf("%s\t%s\t%d\t%s\t%s\t",
				(cou->count[i]).name,
				(cou->count[i]).tel,
				(cou->count[i]).addr);
			printf("\n");
			number++;
		}
	}
 
	if (number == 0)
	{
		printf("此联系人不存在，查找失败！\n");
	}
 
}
//在通讯录里遍历到指定联系人
static int search(people cou, const char *name)
{
	for (int i = 0; i < cou->num; i++)
	{
		if (strcmp(name, (cou->count[i]).name) == 0)
		{
			return i;
		}
	}
 
	return -1;
}
 void name_sort(people cou)//按名字顺序排序所有人
{
	int i = 0;
	int j = 0;
	if (cou->num == 0)
	{
		printf("当前通讯录为空，排序失败！\n");
		return;
	}
 
	for (i = 0; i<cou->num - 1; i++)//冒泡实现联系人的排序
	{
		for (j = 0; j<cou->num - i - 1; j++)
		{
			if (strcmp(cou->count[j].name, (cou->count[j + 1]).name)>0)
			{
				linkman tmp = cou->count[j];
				cou->count[j] = cou->count[j + 1];
				cou->count[j + 1] = tmp;
			}
		}
	}
	printf("排序成功！\n");
}
//添加联系人
void add_linkman(people cou)
{
	if (cou->num == 50)
	{
		printf("通讯录已满\n");
		printf("处理编号超过阈值\n");
		printf("此处已有数据\n");
	}
 
	printf("请输入联系人名字:\n");
	scanf("%s", (cou->count[cou->num]).name);
	printf("请输入联系人电话:\n");
	scanf("%s", (cou->count[cou->num]).tel);
	printf("请输入联系人位置:\n");
	scanf("%s", (cou->count[cou->num]).addr);
 
	cou->num++;
}
 
//删除联系人
void delete_linkman(people cou)
{
	char name[NAME_MAX];
	int i = 0;
 
	if (cou->num == 0)
	{
		printf("处理编号超过阈值！\n");
		return;
	}
 
	printf("请输入要删除人的名字:");
	scanf("%s", &name);
	printf("\n");
 
	int ret = search(cou, name);//找到要删除的联系人
 
	if (ret == -1)
	{
		printf("此处无数据\n");
	}
	else
	{
		for (i = ret; i<cou->num; i++)
		{
			cou->count[i] = cou->count[i + 1];
		}
		cou->num--;
		printf("删除成功！\n");
	}
}
 //修改指定联系人信息
void change_linkman(people cou)
{
	char name[NAME_MAX];
	int i = 0;
	printf("已摘除原有信息，请重新输入:");
	scanf("%s", name);
 
	if (cou->num == 0)
	{
		printf("当前通讯录为空，修改失败！\n");
		return;
	}
 
	int ret = search(cou, name);//找到要修改信息的联系人
 
	if (ret != -1)
	{
	printf("请输入修改联系人名字:\n");
	scanf("%s", (cou->count[cou->num]).name);
	printf("请输入修改联系人电话:\n");
	scanf("%s", (cou->count[cou->num]).tel);
	printf("请输入修改联系人位置:\n");
	scanf("%s", (cou->count[cou->num]).addr);
 
	}
	printf("修改信息成功！\n");
}
//查找联系人
void find_linkman(people cou)
{
	char name[NAME_MAX];
	int i = 0;
 
	printf("请输入要查找人的名字或电话号码:");
	scanf("%s", name);
 
	if (cou->num == 0)
	{
		printf("查无此人！\n");
		return;
	}
 
	_search(cou, name);//找到要查找的联系人
 
}
 
//显示所有联系人信息
void display_linkman(people cou)
{
	int i = 0;
	if (cou->num == 0)
	{
		printf("当前通讯录为空，显示失败！\n");
		return;
	}
 
	for (i = 0; i<cou->num; i++)
	{
		printf("%s\t%s\t%d\t%s\t%s",
			(cou->count[i]).name,
			(cou->count[i]).tel,
			(cou->count[i]).addr);
		printf("\n");
	}
}
void empty_linkman(people cou)//清空所有联系人
{
	cou->num = 0;
}
 
struct stu
{
	char name[15];
};
 
//菜单界面
void menu()
{
	printf("==============通讯录================\n\n");
	printf("==============界面==================\n");
	printf("人数：_50_人          |剩余空间：__人\n编号：__   |姓名：_   |电话：_\n\n");
	printf("操作列表：\n");
	printf("1）排序     2）添加     3）删除\n 4)修改     5）查找     6）退出程序\n请输入操作\n");
}
 
int main()
{
	int opp = 1;
	struct PEOPLE cou;
	cou.num = 0;
 
	while (opp)
	{
		menu();
		printf("请选择>:");
		scanf("%d", &opp);
		switch (opp)
		{
		case 0:
			break;
		case 1:
			add_linkman(&cou);
			break;
		case 2:
			delete_linkman(&cou);
			break;
		case 3:
			find_linkman(&cou);
			break;
		case 4:
			change_linkman(&cou);
			break;
		case 5:
			display_linkman(&cou);
			break;
		case 6:
			empty_linkman(&cou);
			break;
		case 7:
			name_sort(&cou);
			break;
		default:
		{
				   printf("输入有误，请重新输入>：\n");
				   break;
		}
		}
	}
 
	getchar();
	return 0;
} 
 
